# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from aplicacion.models import Cliente, Destino, Reserva

class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nombreC',)

class DestinoAdmin(admin.ModelAdmin):
	list_display = ('nombreD',)

admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Destino, DestinoAdmin)

# Register your models here.