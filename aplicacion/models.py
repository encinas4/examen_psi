# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Cliente(models.Model):
	nombreC=models.CharField(max_length=128, null=True)

	def save(self, *args, **kwargs):
		super(Cliente, self).save(*args, **kwargs)

	def __str__(self):
		return self.nombreC

	def __unicode__(self):
		return "Cliente = %d"% self.id

class Destino(models.Model):
	nombreD=models.CharField(max_length=128, null=False)

	def save(self, *args, **kwargs):
		super(Destino, self).save(*args, **kwargs)

	def __str__(self):
		return self.nombreD

	def __unicode__(self):
		return "Destino = %d"% self.id

class Reserva(models.Model):
	cliente = models.ForeignKey(Cliente, null=True)
	destino = models.ForeignKey(Destino, null=False)
	fechaDeReserva = models.DateTimeField(auto_now_add=True)

	def save(self, *args, **kwargs):
		super(Reserva, self).save(*args, **kwargs)

	def __str__(self):
		return str(self.id)

	def __unicode__(self):
		return "Reserva = %d"% self.id