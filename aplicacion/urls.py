from django.conf.urls import url
from views import index, reservas

urlpatterns = [
    url(r'^$/', index, name='index'),
    url(r'^index/$', index, name='index'),
    url(r'^reserva/$', reservas, name='reservas'),
]