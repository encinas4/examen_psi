# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from aplicacion.models import Cliente, Destino, Reserva

def index(request):
	return render(request, 'aplicacion/index.html')

def reservas(request):
	dest = Destino.objects.filter(nombreD="destino4")
	reservas = Reserva.objects.filter(id__in=dest)

	if len(reservas) == 0:
		return render(request, 'aplicacion/reserva/reserva.html', {'reservas': reservas, 'error': "No hay reservas expedidas."})
	else:
		return render(request, 'aplicacion/reserva/reserva.html', {'reservas': reservas, 'error': False})
