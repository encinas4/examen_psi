import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'proyecto.settings')

import django
django.setup()
from aplicacion.models import Cliente, Destino, Reserva
#from placeorder.models import Order, OrderLine
from django.core.files import File

def populate():
     # First, we will create lists of dictionaries containing the pages
     # we want to add into each category.
     # Then we will create a dictionary of dictionaries for our categories.
     # This might seem a little bit confusing, but it allows us to iterate
     # through each data structure, and add the data to our models.

    datos_clientes = {
      "c1": "cliente1",
      "c2": "cliente2"
    }

    datos_destinos = {
      "d1": "destino1",
      "d2": "destino2",
      "d3": "destino3",
      "d4": "destino4"
    }


    clientes = {}
    destinos = {}

    i=1001
    for cli in sorted(datos_clientes.keys()):
      c = add_cliente(datos_clientes[cli])
      clientes[str(i)] = c
      i+=1

    i=1001
    for dest in sorted(datos_destinos.keys()):
      d = add_destino(datos_destinos[dest])
      destinos[str(i)] = d
      i+=1

    add_reservas(clientes, destinos)

    #imprimimos lo introducido

    for cl in Cliente.objects.all():
      print ("Cliente" + cl.nombreC)

    for de in Destino.objects.all():
      print ("Destino" + de.nombreD)

    for re in Reserva.objects.all():
      if(re.cliente != None):
        print ("Reserva de" + re.cliente.nombreC + "en" + re.destino.nombreD)
      else:
        print ("Reserva para" + re.destino.nombreD)

    

def add_cliente(nombreC):
     c = Cliente.objects.get_or_create(nombreC=nombreC)[0]
     c.save()
     return c

def add_destino(nombreD):
     d = Destino.objects.get_or_create(nombreD=nombreD)[0]
     d.save()
     return d

def add_reservas(clientes, destinos):

    r1 = Reserva.objects.get_or_create(cliente=clientes["1001"], destino=destinos["1001"])[0]
    r2 = Reserva.objects.get_or_create(cliente=clientes["1001"], destino=destinos["1002"])[0]
    r3 = Reserva.objects.get_or_create(cliente=None, destino=destinos["1001"])[0]
    r4 = Reserva.objects.get_or_create(cliente=None, destino=destinos["1004"])[0]
    r5 = Reserva.objects.get_or_create(cliente=clientes["1002"], destino=destinos["1002"])[0]

    r1.save()
    r2.save()
    r3.save()
    r4.save()
    r5.save()
    return


# Start execution here!
if __name__ == '__main__':
    print("Starting Shop population script...")
    populate()
